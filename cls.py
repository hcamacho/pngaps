import os
import sys

import fitsio
import healpy as hp
import matplotlib.pyplot as plt
import numpy as np
import pyccl as ccl
import pymaster as nmt
import scipy
import yaml


def get_nmtbin(elledges, nside):
    if elledges[0] > 0:
        elledges = np.insert(elledges, 0, 0)
    if elledges[-1] < 3 * nside:
        elledges = np.append(elledges, 3 * nside)
    nmtbin = nmt.NmtBin.from_edges(elledges[:-1], elledges[1:])
    print("> Binning effective ells:", nmtbin.get_effective_ells())
    return nmtbin


def get_mask(**config):
    mask = np.zeros(hp.nside2npix(config["nside"]))
    data = fitsio.read(config["mask_path"], ext=1, columns=["hpix", "fracgood"])
    mask[data["hpix"]] = data["fracgood"]
    assert hp.get_nside(mask) == config["nside"], "Wrong mask NSIDE."
    print("> Mask loaded:", config["mask_path"])
    if config["bitmask"]:
        mask[(mask > 0)] = 1.0
    if config["aposize_factor"] > 0:
        aposize = config["aposize_factor"] * np.sqrt(hp.nside2pixarea(config["nside"], degrees=True))
        mask = nmt.utils.mask_apodization(mask, aposize=aposize)
    return mask


def process_mock(mock_id, mask, nmtbin, **config):
    data = {}
    mock_path = os.path.join(
        config["mock_dir"],
        f"lrghp-{config['contam_str']}{config['fnl_str']}-{mock_id}-f1z1.fits",
    )
    nmap = hp.read_map(mock_path)
    assert hp.get_nside(nmap) == config["nside"], "Wrong mock NSIDE."

    if config["get_cell"]:
        delta = nmap / nmap.mean() - 1.0
        delta = nmt.NmtField(np.ones_like(delta), [delta], spin=0)
        data["cell_noise"] = np.array([1.0 / nmap.sum() * 4.0 * np.pi])
        data["cell"] = nmt.compute_coupled_cell(delta, delta) - data["cell_noise"]

    nmap *= mask
    ipgood = mask > 0
    nmean = nmap[ipgood].sum() / mask[ipgood].sum() * hp.nside2npix(config["nside"]) / 4.0 / np.pi

    delta = np.full_like(nmap, -1.0)
    delta[ipgood] = nmap[ipgood] / mask[ipgood] * mask[ipgood].sum() / nmap[ipgood].sum() - 1.0
    delta = nmt.NmtField(mask, [delta], spin=0)

    try:
        nmtwsp = nmt.NmtWorkspace(fname=config["wsp_cache"])
    except RuntimeError:
        nmtwsp = nmt.NmtWorkspace(fl1=delta, fl2=delta, bins=nmtbin)
        nmtwsp.write_to(config["wsp_cache"])
    data["pcl_noise"] = np.array([mask.mean() / nmean])
    data["pcl"] = nmt.compute_coupled_cell(delta, delta)
    data["clb_noise"] = nmtwsp.decouple_cell(np.full_like(data["pcl"], data["pcl_noise"][0]))
    data["clb"] = nmtwsp.decouple_cell(data["pcl"])
    data["pcl"] -= data["pcl_noise"]
    data["clb"] -= data["clb_noise"]
    print("> APS computed, mock:", mock_path)
    return data


def process_mocks(mask, **config):
    nmtbin = get_nmtbin(config["ell_edges"], config["nside"])
    data = process_mock(1, mask, nmtbin, **config)
    for m in range(2, 1001):
        tmp = process_mock(m, mask, nmtbin, **config)
        for k in data:
            data[k] = np.concatenate((data[k], tmp[k]), axis=0)
    data["ell_edges"] = np.array(config["ell_edges"], dtype=int)
    data["lb"] = nmtbin.get_effective_ells()
    data["bpwrwin"] = nmt.NmtWorkspace.from_file(config["wsp_cache"]).get_bandpower_windows()
    header = {k: config[k] for k in ["nside", "lmin", "lmax", "fnl"]}
    header["case"] = f"{config['contam_str']}{config['fnl_str']}"
    fitsio.write(config["out_path"], None, header=header, clobber=True)
    for k in data:
        fitsio.write(config["out_path"], data[k], extname=k)


def get_theory(mask, **config):
    path = os.path.join(config["out_dir"], f"desiCl{config['contam_str']}{config['fnl_str']}-f1z1f1z1.dat")
    ell, cell = np.loadtxt(path, unpack=True)
    assert np.all(ell == ell.astype(int)) and ell[0] == 0 and ell[-1] >= 3 * config["nside"] - 1, "Wrong input ells"
    data = {}
    data["cell"] = cell[: 3 * config["nside"]]
    nmtwsp = nmt.NmtWorkspace.from_file(config["wsp_cache"])
    data["pcl"] = nmtwsp.couple_cell([data["cell"]])[0]
    cell_mask = hp.anafast(mask)
    data["pcl_ic"] = data["pcl"] - cell_mask * data["pcl"][0] / cell_mask[0]
    data["clb"] = nmtwsp.decouple_cell([data["pcl"]])[0]
    data["clb_ic"] = nmtwsp.decouple_cell([data["pcl_ic"]])[0]
    return data


def model_theory(mask, **config):
    cosmo = ccl.Cosmology.read_yaml("cosmo.yaml")
    z, nz, bz = np.loadtxt(config["z_nz_bz_path"], unpack=True)
    nz_interp = scipy.interpolate.interp1d(z, nz, kind="cubic", fill_value=0, bounds_error=False)
    bz_interp = scipy.interpolate.interp1d(z, bz, kind="cubic", fill_value="extrapolate", bounds_error=False)
    z = np.linspace(0, 2, 1024)
    nz, bz = nz_interp(z), bz_interp(z)
    sf = (1.0 / (1 + z))[::-1]

    # TODO: Check if this range is good enough
    ks = np.logspace(-5, 5, 1024)
    transfer_k_fnl = (np.log(ks), 1.0 / ks**2)

    H0 = cosmo.cosmo.params.h / ccl.physical_constants.CLIGHT_HMPC
    omega_m = cosmo.cosmo.params.Omega_c + cosmo.cosmo.params.Omega_b
    transfer_a_fnl = (sf, (bz[::-1] - 1) * 3 * config["fnl"] * config["delta_c"] * omega_m * H0**2 / ccl.growth_factor(cosmo, sf))
    kernel = ccl.get_density_kernel(cosmo, dndz=(z, nz))

    gc_custom = ccl.Tracer()
    gc_custom.add_tracer(cosmo, kernel=kernel, transfer_a=transfer_a_fnl, transfer_k=transfer_k_fnl)

    ells = np.arange(3 * config["nside"])
    data = {}
    data["cell"] = ccl.angular_cl(cosmo, gc_custom, gc_custom, ells, l_limber=config["l_limber"])
    nmtwsp = nmt.NmtWorkspace.from_file(config["wsp_cache"])
    data["pcl"] = nmtwsp.couple_cell([data["cell"]])[0]
    cell_mask = hp.anafast(mask)
    data["pcl_ic"] = data["pcl"] - cell_mask * data["pcl"][0] / cell_mask[0]
    data["clb"] = nmtwsp.decouple_cell([data["pcl"]])[0]
    data["clb_ic"] = nmtwsp.decouple_cell([data["pcl_ic"]])[0]
    return data


if __name__ == "__main__":
    with open(sys.argv[1], "r") as file:
        config = yaml.safe_load(file)

    if config["fnl"] == 0:
        config["fnl_str"] = "zero"
    elif config["fnl"] > 0:
        config["fnl_str"] = f"po{config['fnl']:d}"
    else:
        raise NotImplementedError("Negative fNL not implemented")

    config.setdefault("contam_str", "")
    config.setdefault("lmin", config["ell_edges"][0])
    config.setdefault("lmax", config["ell_edges"][-1])
    config.setdefault("get_cell", False)
    config.setdefault("wsp_cache", os.path.join(config["out_dir"], f"wsp_{os.path.basename(config['mask_path'])}"))
    config.setdefault("recompute", False)
    config.setdefault("out_path", os.path.join(config["out_dir"], f"cls_{config['contam_str']}{config['fnl_str']}.fits"))

    if config["recompute"] and os.path.exists(config["wsp_cache"]):
        os.remove(config["wsp_cache"])

    mask = get_mask(**config)
    if config["recompute"] or not os.path.exists(config["out_path"]):
        process_mocks(mask, **config)
    data = fitsio.FITS(config["out_path"])
    # data_theory = get_theory(mask, **config)
    data_theory = model_theory(mask, **config)

    fig, axes = plt.subplots(ncols=3, nrows=2, sharex=True, sharey="row", gridspec_kw={"height_ratios": [2, 1]})
    ylabel = [r"$\hat{C}_\ell$", r"$\tilde{C}_\ell$", r"$\hat{C}_b$"]
    title = ["full sky", "masked and coupled", r"masked, uncoupled and binned"]
    ylabel_diff = [
        r"$(\hat{C}_\ell - C^{\rm theory}_\ell) / \sigma[\hat{C}_\ell]$",
        r"$(\hat\tilde{C}_\ell - \tilde{C}^{\rm theory}_\ell) / \sigma[\hat\tilde{C}_\ell]$",
        r"$(\hat{C}_b - C^{\rm theory}_b) / \sigma[\hat{C}_b]$",
    ]
    for ii, aps in enumerate(["cell", "pcl", "clb"]):
        ax = axes[0][ii]
        estimate = data[aps].read()
        noise_mean = data[f"{aps}_noise"].read().mean(axis=0)
        theory = data_theory[aps]
        if aps != "cell":
            theory_ic = data_theory[f"{aps}_ic"]
        mean, std = estimate.mean(axis=0), estimate.std(axis=0)
        if aps == "clb":
            x = data["lb"].read()
        else:
            x = np.arange(len(theory))
            noise_mean = np.full_like(mean, noise_mean)
        mask = np.ones_like(x, dtype=bool)
        print(aps, x.shape, theory.shape)
        x, theory, mean, std, noise_mean = x[mask], theory[mask], mean[mask], std[mask], noise_mean[mask]
        ax.plot(x, theory, label="Input LN theory w/o IC")
        if aps != "cell":
            ax.plot(x, theory_ic, label="Input LN theory w/ IC")
        if aps == "clb":
            ax.errorbar(x, mean, yerr=std, ls="", marker="o", ms=1, mfc="none", color="k")
        else:
            ax.plot(x, mean, label="mean of mocks", color="k")
            ax.fill_between(x, mean - std, mean + std, alpha=0.5, color="k")
        ax.plot(x, noise_mean, ls=":", label="noise", color="k")
        ax.set(yscale="log", xscale="log", xlim=(2e-1, 3 * config["nside"] + 1), ylabel=ylabel[ii])
        ax.set_title(title[ii], fontsize="medium")
        ax.axvspan(ax.get_xlim()[0], config["lmin"], hatch="///", edgecolor="gray", facecolor="none")
        ax.axvspan(config["lmax"], ax.get_xlim()[1], hatch="///", edgecolor="gray", facecolor="none")
        if aps == "pcl":
            ax.legend(loc="best", frameon=False, fontsize="medium")
        ax = axes[1][ii]
        if aps == "clb":
            ax.errorbar(x, (mean - theory) / std, yerr=1, marker="o", ms=1)
            ax.errorbar(x, (mean - theory_ic) / std, yerr=1, marker="o", ms=1, mfc="none")
        elif aps == "pcl":
            ax.plot(x, (mean - theory) / std)
            ax.plot(x, (mean - theory_ic) / std)
        else:
            ax.plot(x, (mean - theory) / std)
        ax.set(ylim=(-2.5, 2.5), xlim=(2e-1, 3 * config["nside"] + 1), ylabel=ylabel_diff[ii], yticks=[-2, -1, 0, 1, 2])
        ax.grid(axis="y")
        ax.axvspan(ax.get_xlim()[0], config["lmin"], hatch="///", edgecolor="gray", facecolor="none")
        ax.axvspan(config["lmax"], ax.get_xlim()[1], hatch="///", edgecolor="gray", facecolor="none")
    fig.supxlabel(r"$\ell$, multipole", fontsize="medium")
    fig.set_size_inches(9, 5)
    fig.set_layout_engine("constrained")
    fig.savefig(f"cls_{config['contam_str']}{config['fnl_str']}.png", bbox_inches="tight", dpi=100)
